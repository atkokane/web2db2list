package com.lge.webserv2db;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button fetchButton;
    FetchData asyncTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fetchButton = findViewById(R.id.fetchBtn);
        fetchButton.setOnClickListener(this);
        asyncTask = new FetchData(this);
    }



    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fetchBtn) {
            asyncTask.execute();
            Toast.makeText(getApplicationContext(), "List will be launched when data available from webservice...",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
