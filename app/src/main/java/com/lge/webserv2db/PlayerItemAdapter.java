package com.lge.webserv2db;


import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by kokane.tulshiram on 28-05-2018.
 */

public class PlayerItemAdapter extends CursorAdapter {

    final String TAG = PlayerItemAdapter.class.getSimpleName();
    Context mContext;

    class ViewHolder {
        TextView name, desc;
        ImageView dp;
    }

    public PlayerItemAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        mContext = context;
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = getViewHolder(view);
        Log.d(TAG, "this is value" + cursor.getString(cursor.getColumnIndexOrThrow(FillDatabase.PlayerName)));
        viewHolder.name.setText(cursor.getString(cursor.getColumnIndexOrThrow(FillDatabase.PlayerName)));//cursor.getString(1));
        viewHolder.desc.setText(cursor.getString(cursor.getColumnIndexOrThrow(FillDatabase.desc)));//cursor.getString(3));
        viewHolder.dp.setImageResource(R.drawable.ic_launcher_foreground);
//        dp.setImageDrawable(R.drawable.ic_launcher_foreground);
//        dp.setImageURI(cr.getColumnIndexOrThrow(FillDatabase.uri));
    }

/*    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(TAG, "im here");
        ViewHolder vh;
        if (convertView == null) {
            Log.e(TAG, "Convertview is null");
            convertView = LayoutInflater.from(mContext).inflate(R.layout.fragment_item, null);
            vh = getViewHolder(convertView);
            convertView.setTag(vh);
        }
        bindView(convertView, mContext, cr);
        return convertView;
    }*/

    private ViewHolder getViewHolder(View convertView) {
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.name = convertView.findViewById(R.id.name);
        viewHolder.desc = convertView.findViewById(R.id.description);
        viewHolder.dp = convertView.findViewById(R.id.dp_icon);
        return viewHolder;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        Log.d(TAG, "im here");
        return LayoutInflater.from(mContext).inflate(R.layout.fragment_item, null);
    }
}
