package com.lge.webserv2db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by kokane.tulshiram on 22-05-2018.
 */

public class FillDatabase extends SQLiteOpenHelper {

    final static private String DBName = "JSONDb";
    static final private String TableName = "Footballers";
    private static final String id = "_id";
    public static final String PlayerName = "PlayerName";
    public static final String uri  = "dpURI";
    public static final String desc = "Descr";


    SQLiteDatabase jsonDB;

    public FillDatabase(Context context) {
        super(context, DBName, null, 1);
        openDB();
    }

    public void openDB() {
        jsonDB = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create table "+TableName +
                " ( "
                        + id+ " INTEGER PRIMARY KEY, "
                        + PlayerName + " TEXT, "
                        + uri +" TEXT, "
                        + desc + " TEXT"
                        + ")"
                    );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.delete(TableName, null, null);
        onCreate(db);
    }

    public boolean insertIntoDB(ContentValues bundle) {
        if (bundle != null && jsonDB.isOpen()) {
            Log.d("DBHelper", "inserting bundle");
           return jsonDB.insert(TableName, null, bundle) > 0;
        }
        return false;
    }

    public Cursor getCursor() {
        Cursor cr = null;
        if (!jsonDB.isOpen()) {
            Log.d("DBHelper", "DB is not open");
            openDB();
        }
        String[] columns = new String[] {id, PlayerName, uri,desc};
        cr = jsonDB.query(TableName, columns,null, null, null,null,null);
       // cr.moveToPosition(2);
        return cr;
    }
}
