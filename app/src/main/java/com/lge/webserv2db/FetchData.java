package com.lge.webserv2db;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by kokane.tulshiram on 22-05-2018.
 */

public class FetchData extends AsyncTask<Void, Integer, String> {
    URL url;
    private static final String TAG = FetchData.class.getSimpleName();
    URLConnection httpURLConnection;
    InputStream inputStream;
    BufferedReader bufferedReader;
    FillDatabase fillDatabase;
    Context mainActivity;

    public FetchData(Context context) {
        fillDatabase = new FillDatabase(context);
        mainActivity = context;
    }

    @Override
    protected String doInBackground(Void... voids) {
        String jsonData = "";
        String data = "";
        String res = "";
        try {
            url= new URL("https://api.myjson.com/bins/oyx4v");
            httpURLConnection = url.openConnection();
            inputStream = httpURLConnection.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            if (bufferedReader != null) {
                while (data != null) {
                    data = bufferedReader.readLine();
                    jsonData = jsonData + data;
                }
            }
            if (parseJSONData(jsonData)) res = "Success";

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        Log.d(TAG,"Progress : "+values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.d(TAG, "Data fetch got "+s);
        if (s.equals("Success")) {
            Intent listActivity = new Intent(mainActivity, PlayersList.class);
            mainActivity.startActivity(listActivity);
        }


    }

    boolean parseJSONData(String s) {
        ContentValues contentValues = new ContentValues();
        boolean success = false;
        try {
            JSONArray jsonArray = new JSONArray(s);
            for (int i = 0;i<jsonArray.length();i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                contentValues.put(FillDatabase.PlayerName, (jsonObject.get("name")).toString());
                contentValues.put(FillDatabase.uri, (jsonObject.get("image")).toString());
                contentValues.put(FillDatabase.desc, (jsonObject.get("desc")).toString());
                Log.d(TAG,"Name is "+jsonObject.get("name"));
                fillDatabase.insertIntoDB(contentValues);
                success = true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return success;
    }
}
