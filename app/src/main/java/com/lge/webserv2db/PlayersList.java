package com.lge.webserv2db;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PlayersList extends Activity {


    Fragment playerListFragment;
    FragmentManager fragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players_list);
        fragmentManager = getFragmentManager();
        playerListFragment = fragmentManager.findFragmentById(R.id.playerlist);
    }
}
