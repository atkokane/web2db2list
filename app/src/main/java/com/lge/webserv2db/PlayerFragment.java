package com.lge.webserv2db;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


public class PlayerFragment extends Fragment {
    ListView mList;

    public PlayerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);
        mList = view.findViewById(android.R.id.list);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        // Set the adapter
        Context context = getActivity();
        FillDatabase fillDatabase = new FillDatabase(context);
        PlayerItemAdapter playListAdapter = new PlayerItemAdapter(context, fillDatabase.getCursor(), false);
        mList.setAdapter(playListAdapter);
        super.onActivityCreated(savedInstanceState);
    }
}